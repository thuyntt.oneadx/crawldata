const Hidemyacc = require("./hidemyacc");
const hidemyacc = new Hidemyacc();
const puppeteer = require("puppeteer-core");
const path = require("path");
const fs = require("fs");
const { fork } = require("child_process");
const { text } = require("express");
const { log } = require("console");
const delay = (timeout) =>
  new Promise((resolve) => setTimeout(resolve, timeout));

!(async () => {
  try {
    const proxy = JSON.stringify({
      host: "oneadx.ddns.net",
      mode: "http",
      password: "",
      port: 54017,
      username: "",
    });
    const profile = {
      os: "win",
      name: "La So Tu Vi",
      notes: "example test",
      browser: "chrome",
      proxy: proxy,
    };
    let user = await hidemyacc.create(profile);
    let profileId = await user.data.id;
    const response = await hidemyacc.start(profileId);
    if (response.code !== 1) {
      throw new Error("Khong mo duoc trinh duyet");
    }

    const browser = await puppeteer.connect({
      browserWSEndpoint: response.data.wsUrl,
      defaultViewport: null,
      slowMo: 60,
      timeout: 0,
      protocolTimeout: 120000,
    });

    const pages = await browser.pages();
    let page;

    if (pages.length) {
      page = pages[0];
    } else {
      page = await browser.newPage();
    }
    await page.goto("http://cohoc.net/", {
      waitUntil: "networkidle2",
      timeout: 30000,
    });

    await page.click('a[href="menh-ly-van-nien.html"]');
    await delay(1000);

    const [genderValues,yearValues,monthValues,dayValues,hourValues,clickSequences] = await GetData(page);

    const maxLoadTime = 5000;
    await CrawlData( page,genderValues,yearValues,monthValues,dayValues,hourValues,clickSequences,maxLoadTime);

    await hidemyacc.stop(profileId);
  } 
  catch (error) {
    if (error.message.includes("Session closed")) {
      console.log("Session closed, đang thử kết nối lại.");
      try {
        await browser.disconnect();
      } catch (disconnectError) {
        console.error("Lỗi khi đóng phiên trình duyệt:", disconnectError);
      }
      const browser = await puppeteer.connect({
        browserWSEndpoint: response.data.wsUrl,
        defaultViewport: null,
        slowMo: 60,
        timeout: 0,
        protocolTimeout: 30000,
      });
    } else if (
      error.message.includes(
        "Protocol error (Runtime.callFunctionOn): Session closed"
      )
    ) {
      console.log("Trang có thể đã bị đóng. Thử mở lại hoặc xử lý lỗi.");
      await page.reload();
    } else {
      console.error("Lỗi:", error);
    }
  }
})();


async function GetData(page) {
  try {
    const [genderValues, yearValues, monthValues, dayValues, hourValues] = await Promise.all([
      page.evaluate(() => {
        const genderInputs = Array.from(document.querySelectorAll('input[name="GioiTinh"][type="radio"]'));
        return genderInputs.map((input) => input.value);
      }),
      page.evaluate(() => {
        const select = document.querySelector("select#ddlNam");
        const options = select.querySelectorAll("option");
        return Array.from(options).map((option) => option.value);
      }),
      page.evaluate(() => {
        const select = document.querySelector("select#ddlThang");
        const options = select.querySelectorAll("option");
        return Array.from(options).map((option) => option.value);
      }),
      page.evaluate(() => {
        const select = document.querySelector("select#ddlNgay");
        const options = select.querySelectorAll("option");
        return Array.from(options).map((option) => option.value);
      }),
      page.evaluate(() => {
        const select = document.querySelector("select#ddlGio");
        const options = Array.from(select.options);
        return options.map((option) => {
          return { value: option.value, text: option.innerText };
        });
      }),
    ]);

    const clickSequences = [];

    for (let gender = 0; gender < genderValues.length; gender++) {
      for (let year = 0; year < yearValues.length; year++) {
        for (let month = 0; month < monthValues.length; month++) {
          let daysInMonth;
          if ([4, 6, 9, 11].includes(month + 1)) {
            daysInMonth = 30;
          } 
          else if (month === 1) {
            if ((year % 4 === 0 && year % 100 !== 0) || year % 400 === 0) {
              daysInMonth = 29;
            } else {
              daysInMonth = 28;
            }
          } else {
            daysInMonth = 31;
          }
          for (let day = 0; day < daysInMonth; day++) {
            for (let hour = 0; hour < hourValues.length; hour++) {
              clickSequences.push({
                genderIndex: gender,
                yearIndex: year,
                monthIndex: month,
                dayIndex: day,
                hourIndex: hour
              });
            }
          }
        }
      }
    }

    return [genderValues,yearValues,monthValues,dayValues,hourValues,clickSequences];
  } catch (error) {
    console.log("Lỗi trong quá trình lấy dữ liệu: " + error);
  }
}

async function CrawlData(page, genderValues, yearValues, monthValues, dayValues, hourValues, clickSequences, maxLoadTime) {
  try {
    let currentUrl = await page.url();
    const timing = await page.evaluate(() => {
      return {
        domContentLoadedEventEnd: window.performance.timing.domContentLoadedEventEnd,
        navigationStart: window.performance.timing.navigationStart
      };
    });

    let loadTime = timing.domContentLoadedEventEnd - timing.navigationStart;

    if (currentUrl.includes("menh-ly-van-nien.html") && clickSequences) {
      for (const sequence of clickSequences) {
        await handleSequence(page, sequence, genderValues, yearValues, monthValues, dayValues, hourValues, loadTime, maxLoadTime,currentUrl);
      }
    } else {
      await page.reload();
      return await CrawlData(page, genderValues, yearValues, monthValues, dayValues, hourValues, clickSequences, maxLoadTime);
    }
  } catch (error) {
    console.error("Lỗi trong quá trình crawl: " + error);
    if (error.message.includes('Runtime.callFunctionOn timed out')) {
      await handleTimeoutError(page);
    }
  }
}

async function handleSequence(page, sequence, genderValues, yearValues, monthValues, dayValues, hourValues, loadTime, maxLoadTime,currentUrl) {
  try{
    const { genderIndex, yearIndex, monthIndex, dayIndex, hourIndex } = sequence;
    
    await page.$eval(
      `input[name="GioiTinh"][type="radio"][value="${genderValues[genderIndex]}"]`,
      (radio) => radio.click()
    );
    await page.select("select#ddlNam", yearValues[yearIndex]);
    await page.select("select#ddlThang", monthValues[monthIndex]);
    await page.select("select#ddlNgay", dayValues[dayIndex]);
    await page.select("select#ddlGio", hourValues[hourIndex].value);
    await delay(3000);
    await page.click('input[name="btGiaiDoan"]');
    currentUrl = await page.url();
    await delay(3000);

    const error = await page.$("span#lbMess");

    if (!error) {
      await handleNoError(page, genderIndex, genderValues, yearIndex, yearValues, monthIndex, monthValues, dayIndex, dayValues, hourIndex, hourValues,currentUrl);
    } else {
      await handleSequenceError(page, currentUrl, loadTime, maxLoadTime);
    }
  }
  catch(error){
    console.log("Loi: " + error);
  }
}

async function handleNoError(page, genderIndex, genderValues, yearIndex, yearValues, monthIndex, monthValues, dayIndex, dayValues, hourIndex, hourValues,currentUrl) {
  try{
    if (currentUrl.includes("/404.html?" || loadTime > maxLoadTime)) {
      await delay(35000);
    }
    const genderText = genderValues[genderIndex] === "rdNam" ? "Nam" : "Nu";
    await WriteDataToFile(
      page,
      genderText,
      yearValues[yearIndex],
      monthValues[monthIndex],
      dayValues[dayIndex],
      hourValues[hourIndex].text.replace(/\s+/g, "")
    );
    
    await delay(3000);
    await page.waitForSelector('a[href="http://cohoc.net/menh-ly-van-nien.html"]');
    await page.click('a[href="http://cohoc.net/menh-ly-van-nien.html"]');
    currentUrl = await page.url();
    await delay(5000);
  }
  catch(error){
    console.log("Loi trong qua trinh ghi du lieu vao file: " + error);
  }
}

async function handleSequenceError(page, currentUrl) {
  try{
    await delay(2000);
    await page.click('input[name="btGiaiDoan"]');
    currentUrl = await page.url();
    await delay(3000);
  }
  catch(error){
    console.log("Loi: " + error);
  }
}

async function handleTimeoutError(page) {
  try{
    await delay(60000);
    await page.click('a[href="menh-ly-van-nien.html"]');
    await delay(3000);
    return await CrawlData(page, genderValues, yearValues, monthValues, dayValues, hourValues, clickSequences, maxLoadTime);
  }
  catch(error){
    console.log("Loi trong khi timeout: " + error);
  }
}

async function WriteDataToFile(page, gender, day, month, year, hour) {
  try {
    const html = await page.content();
    const folderPath = "data";
    if (!fs.existsSync(folderPath)) {
      fs.mkdirSync(folderPath);
    }
    const customFilename = `${gender}-${day}-${month}-${year}-${hour}.html`;
    const filePath = path.join(folderPath, customFilename);
    fs.writeFileSync(filePath, html, "utf-8");
    console.log(
      `Dữ liệu đã được ghi vào tệp ${customFilename} trong thư mục ${folderPath}`
    );
  } catch (error) {
    console.error("Lỗi trong quá trình ghi file:", error);
  }
}